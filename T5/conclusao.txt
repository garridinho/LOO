\begin{table}[H]
  \centering
  \caption{Resistências internas e coeficientes de auto-indução das bobinas}
  \begin{tabular}{c||c c c| c c}
    \toprule
    \hline
     $n^{o} espiras$ & $f_{ress}(Hz)$  & $L(mH)$         & $R_i(\Omega)$ \\\hline 
      1800(D)       &  $210 \pm 1$   & $80.1 \pm 0.1$  & $7.59$   \\ 
      1800(E)       &  $177 \pm 2$   & $112.7\pm 0.3$  & $    $   \\
      3600          &  $100 \pm 1$   & $353.2\pm 0.8$  & $    $   \\
      600           &  $525 \pm 7$   & $12.83\pm 0.04$ & $    $   \\
      300           &  $1321\pm 3$   & $2.025\pm 0.002$& $    $   \\
    \hline
    \bottomrule
      \end{tabular}
  \label{tab:coefresist}
\end{table}
