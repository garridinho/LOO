#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define EXT ".txtbl"
#define BSZ 150

typedef struct list list;

typedef union car {
   double dval;
   void *pval;
} car;

struct list {
   car data;
   char type;
   list *next;
};

/*funções das listas de doubles*/
void l_push(list ** lst, car data, char type)
{
   while (*lst != NULL) {
      lst = &(*lst)->next;
   }
   *lst = malloc(sizeof(list));
   (*lst)->data = data;
   (*lst)->next = NULL;
   (*lst)->type = type;
}

void l_rm(list * lst)
{
   if (lst != NULL) {
      if (lst->next != NULL)
	 l_rm(lst->next);
      if (lst->type == 1)
	 l_rm(lst->data.pval);
      free(lst);
   }
}

int l_sz(list * lst)
{
   int i;
   for (i = 0; lst != NULL; i++)
      lst = lst->next;
   return i;
}


/*só deve ser utilizada um listas de doubles.
 * principalmente para testes*/
void l_print(list * lst)
{
   while (lst != NULL) {
      switch (lst->type) {
         case 1:
	 l_print(lst->data.pval);
         break;
         case 0:
	 printf("%f\n", lst->data.dval);
         break;
         case 2:
         puts(lst->data.pval);
         break;
      }
      puts("===");
      lst = lst->next;
   }
}

list **l_split(list * lst, int parts)
{
   int i, j, s = l_sz(lst) / parts + 1;
   list **retl = NULL;
   retl = malloc(sizeof(list **) * parts);
   retl[0] = lst;
   printf("%d\n", s);

   for (j = 1; j < parts; j++) {
      lst = retl[j - 1];
      for (i = 1; lst->next != NULL && i < s; i++) {
	 lst = lst->next;
	 printf("i %d,ptr %p \n", i, lst);
      }
      retl[j] = lst->next;
      lst->next = NULL;
      printf("j %d,ptr %p \n", j, retl[j]);
   }
   return retl;
}


/*função de substituição de extensão de um ficheiro*/
char *extsub(char *s, char *ext)
{
   char *r;
   int i;

   for (i = 0; s[i] != 0 && s[i] != '.'; i++);

   r = malloc(i + 1 + strlen(ext));

   strcpy(r, s);
   strcpy(r + i, ext);

   return r;
}

/* diz o numero de linhas num ficheiro*/
int getlns(FILE * fp)
{
   char c;
   int i = 0;
   while ((c = fgetc(fp)) != EOF)
      if (c == '\n')
	 i++;
   return i + 1;
}

/*processa cada linha numa lista de doubles*/
int parseln(FILE * fp, list ** lst)
{
   car tmp;
   char buf[BSZ], *endptr, *ptr;

   endptr = buf;
   if (fgets(buf, BSZ, fp) == NULL)
      return 1;
   do {
      ptr = endptr;
      tmp.dval = strtod(ptr, &endptr);
      if (endptr != ptr)
	 l_push(lst, tmp, 0);
   }
   while (ptr != endptr);
   return 0;
}

/*proocessa um ficheiro numa lista de listas de doubles*/
void parsefile(FILE * fp, list ** lst)
{
   list *tlst = NULL;
   car lpt;
   while (!parseln(fp, &tlst)) {
      lpt.pval = tlst;
      l_push(lst, lpt, 1);
      tlst = NULL;
   }
}

void printfile(FILE * fp, list * lst, list * fmt)
{
}

char **parsefmt(FILE * fp, list * lst)
{
   char buf[BSZ];
   int i,j;
   car tmp;

   fgets(buf, BSZ, fp);
   for (i = 0; buf[i] != '\n'; i++);
   buf[i] = 0;
   i = 0;

   while (buf[i] != 0) {
      j=i;
      do {
      i++;
      } while (buf[i - 1] != 'f' && buf[i - 1] != 0);
      tmp.pval = malloc(sizeof(char)*(i-j)+1);
      memcpy();
      l_push(&lst,tmp,2);
   }
   for (i = 0; buf[i] != 0; i++)
      printf("%d,%c\n", i, buf[i]);
}

int main(int argc, char **argv)
{
   FILE *fpr, *fpw;
   char *n;
   list *lst, *fmtl;
   lst = fmtl = NULL;
   list **llist;

   switch (argc) {
   case 1:
      {
	 fpr = stdin;
	 fpw = stdout;
	 break;
      }
   case 2:
      {
	 fpr = fopen(argv[1], "r");
	 //fpw = fopen (n=extsub(argv[1], EXT),"w");
	 //free(n);
	 break;
      }
   default:
      {
	 puts("?");
	 exit(-1);
      }
   }

   if (fpr == NULL) {
      puts("?");
      exit(-1);
   }

   rewind(fpr);

   parsefmt(fpr, fmtl);

   //parsefile(fpr, &lst);
   //llist = l_split(lst, 3);
   //puts("done");
   l_print(fmtl);
   l_rm(lst);
   return 0;
}
